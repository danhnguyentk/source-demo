
import 'reflect-metadata'
import 'zone.js/dist/zone'

import { BrowserModule } from '@angular/platform-browser'
import { platformBrowserDynamic} from '@angular/platform-browser-dynamic'
import { NgModule }  from '@angular/core' 

import { AppComponent} from './app/app.component'

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule
	],
	providers: [],
	bootstrap: [AppComponent]
})

export class AppModule {}

platformBrowserDynamic().bootstrapModule(AppModule)
	.catch(error => { console.log(error)});