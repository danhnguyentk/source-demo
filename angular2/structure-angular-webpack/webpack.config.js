
var webpack = require('webpack'); // to access built-in plugins
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: [
		'./src/main.ts'
	] ,

	output: {
		path: './dist',
		filename: 'app.bundle.js'
	},

	module: {
		preLoaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'jshint-loader'
			}
		],

		loaders: [
			{test: /\.ts$/, loader: 'ts'},
			{test: /\.css$/, loader: 'css-loader!autoprefixer-loader'},
			{test: /\.scss$/, loader: 'css-loader!autoprefixer-loader!sass-loader'},
			{test: /\.html$/, loader: 'html-loader'},
			{test: require.resolve("lodash"), loader: 'expose?_'},
			{test: require.resolve("jquery"), loader: 'expose?$!expose?jQuery'},
		]
	},

	resolve: {
		extensions: ['', '.js', '.ts', '.css', '.scss', '.html', '.jade']
	},

	plugins: [
		new HtmlWebpackPlugin({
			template: './src/index.html',
			inject: true
		})
	],

	devServer: {
		contentBase: './dist',
		inline: true,
		port: 8000
	},

	jshint: {
	       // any jshint option http://www.jshint.com/docs/options/
	       // i. e.
	       camelcase: true,

	       // jshint errors are displayed by default as warnings
	       // set emitErrors to true to display them as errors
	       emitErrors: false,

	       // jshint to not interrupt the compilation
	       // if you want any file with jshint errors to fail
	       // set failOnHint to true
	       failOnHint: false,

	       // custom reporter function
	       reporter: function(errors) { }
	   }
};

