
var p = new Promise (function(resolve, reject) {
	setTimeout(function() {
		resolve(10);
	}, 1000);
	// resolve('Success');
	// reject(new Error('error'));
	// reject('error');
})

p.then(function(value) {
		console.log(value); // return 10
		return 20;
	}, function(error) {
		console.log(error);
	})
	.then(function(num) {
		console.log(num); // 20
		return  new Promise (function(resolve, reject) {
			resolve(30) });
	})
	.then(function(num) {
		console.log(num); // 30
	})
.catch(function(error) {
		console.log(error);
	});


var req1 = new Promise(function(resolve, reject) { 
	// A mock async action using setTimeout
	setTimeout(function() { reject('First!'); }, 4000);
});
var req2 = new Promise(function(resolve, reject) { 
	// A mock async action using setTimeout
	setTimeout(function() { resolve('Second!'); }, 3000);
});
Promise.all([req1, req2]).then(function(results) {
	console.log('Then: ', results); // first
}).catch(function(err) {
	console.log('Catch: ', err);
});


var req1 = new Promise(function(resolve, reject) { 
	// A mock async action using setTimeout
	setTimeout(function() { resolve('First!'); }, 400);
});
var req2 = new Promise(function(resolve, reject) { 
	// A mock async action using setTimeout
	setTimeout(function() { resolve('Second!'); }, 3000);
});
Promise.race([req1, req2]).then(function(one) {
	console.log('Then: ', one); // first
}).catch(function(one, two) {
	console.log('Catch: ', one);
}); 
