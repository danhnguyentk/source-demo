

function classDecorator(target) {
	console.log('Class')
	console.log(target); // class the decorator is declared on
}

function methodDecorator(isBoolean: boolean) {
	return (target: Object, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => {
		console.log('Method');
		console.log(target); // propotype of the class
		console.log(propertyKey); // the name of the method
		console.log(descriptor); // property descriptor
	}
}

function propertyDecorator(target, propertyKey) {
	console.log('Property');
	console.log(target); // the propertype of the class
	console.log(propertyKey); // name of property
} 

function parameterDecorator(target, propertyKey, parameterIndex) {
	console.log('Parameter')
	console.log(target); // propotype of the class
	console.log(propertyKey); // the name of the method
	console.log(parameterIndex); // index of parameter in the list of the function's parameters 
}

@classDecorator
class MySuper {

	@propertyDecorator
	name: string;

	@methodDecorator(true)
	call(@parameterDecorator myPara, x, y) { }
}
console.log(MySuper.isSuperhero);


//property decorators 
function Override(label: string) {
	return function (target: any, key: string) {
		Object.defineProperty(target, key, {
			configurable: false,
			get: () => label
		});
	}
}

class Test {
	// @Override('test')
	name: string = 'pat';
}
let t = new Test();
console.log(t.name)


function ReadOnly(target: any, key: string) {
	Object.defineProperty(target, key, {writable: false});
}
class Test2 {
	@ReadOnly
	name: string;
}
var t2 = new Test2();
t2.name = 'jan';
console.log(t2.name);