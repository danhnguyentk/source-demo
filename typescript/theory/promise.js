

// function doSomething() {
// 	return {
// 		then: function(callback) {
// 			var value = 42;
// 			callback(value);
// 		}
// 	}
// }
// doSomething().then(console.log);


// function Promise1(fn) {
// 	var callback = null;

// 	this.then = function(cb) {
// 		callback = cb;
// 	}

// 	function resolve(value) {
// 		setTimeout(function() {
// 			callback(value);
// 		}, 1);
		
// 	}

// 	fn(resolve);
// }

function Promise(fn) {
  var state = 'pending';
  var value;
  var deferred;

  function resolve(newValue) {
    value = newValue;
    state = 'resolved';

    if(deferred) {
      handle(deferred);
    }
  }

  function handle(onResolved) {
    if(state === 'pending') {
      deferred = onResolved;
      return;
    }

    onResolved(value);
  }

  this.then = function(onResolved) {
    handle(onResolved);
  };

  fn(resolve);
}



function doSomething() {
	return new Promise(function(resolve) {
		var value = 42;
		resolve(42);
	})
}

function Promise(fn) {
	var state = 'pending';
	var value;
	var deferred;

	function resolve(newValue) {
		value = newValue;
		state = 'resolved';

		if(deferred) {
			handle(deferred);
		}
	}	

	function handle(handler) {
		if(state === 'pending') {
			deferred = handler;
			return ;
		}

		if(!handler.onResolved) {
			handler.onResolved(value);
			return;
		}

		var ret = handler.onResolved(value);
		handler.resolve(ret);
	}

	this.then = function(onResolved) {
		return new Promise(function(resolve) {
			handle({
				onResolved: onResolved,
				resolve: resolve
			});
		});
	};

	fn(resolve);

}
var promise = doSomething();

promise.then(function(value) {
  console.log('first value:', value);
  return 50;
}).then(function(value) {
	console.log('second value:', value);
	return 60;
}).then(function(value) {
	console.log('Third value:', value);
	return 70;
});